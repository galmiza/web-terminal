//
// Two functions:
// + ansi2html
// + escapeHtml
//

/*
 * Converts unix shell text that contains color information into html
 * Replaces [{attr};{fg};{bg}m by style <span>
 * Specification of color codes: https://www.linuxjournal.com/article/8603
 */
var ansi2html = function(text) {

  var s = "\\[([0-9]*);?([0-9]*);?([0-9]*)m";
  var r1 = new RegExp(s,'gm'); // to get groups
  var r2 = new RegExp(s,''); // to get individual group details
  var m1 = text.match(r1);

  // Closing style
  text = text.replace(/\[39m/g,"</span>");

  var getStyleFromCode = function(code) {
    var style="";
    switch (code) {
      case 0:  style = "font-weight:normal;" ; break;
      case 1:  style = "font-weight:bold;" ; break;
      case 30: style = "color:black;" ; break;
      case 31: style = "color:red;" ; break;
      case 32: style = "color:green;" ; break;
      case 33: style = "color:yellow;" ; break;
      case 34: style = "color:blue;" ; break;
      case 35: style = "color:magenta;" ; break;
      case 36: style = "color:cyan;" ; break;
      case 37: style = "color:white;" ; break;
      case 40: style = "background-color:black;" ; break;
      case 41: style = "background-color:red;" ; break;
      case 42: style = "background-color:green;" ; break;
      case 43: style = "background-color:yellow;" ; break;
      case 44: style = "background-color:blue;" ; break;
      case 45: style = "background-color:magenta;" ; break;
      case 46: style = "background-color:cyan;" ; break;
      case 47: style = "background-color:white;" ; break;

      // Inspired from foverer rendering on MacOS terminal
      case 90: style = "color:darkgrey;" ; break;
    }
    return style;
  }
  if (m1) {
    m1.map(function(i) {
      var m2 = i.match(r2);
      var style = "";
      style += getStyleFromCode(parseInt(m2[1]));
      style += getStyleFromCode(parseInt(m2[2]));
      style += getStyleFromCode(parseInt(m2[3]));
      text = text.replace(m2[0],"<span style='"+style+"'>");
      console.log(m2[0],style);
    });
  }
  return text;
}

/*
 * Espace HTML from string
 * Source: https://stackoverflow.com/questions/24816/escaping-html-strings-with-jquery
 */
var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  '/': '/',
  "'": "'",
  '`': '`',
  '=': '='
};
var escapeHtml = function(string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}