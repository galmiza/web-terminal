//
// Define behavior on key press
// + ENTER (submit content of textarea as command)
// + shift+ENTER (adds a newline at cursor position)
// + UP/DOWN (browse history to textarea)
// + alt+UP/DOWN (move cursor in textarea)
// Define behavior on resize
// + #terminal height = window - .banner - #textarea
// Handle click events
// + authenticate (send authentication token to server)
// + _clear (clear terminal view)
// + toggleMenu (toggle visibility of the menu)
// Init socket.io
// + channel stdout (append lines to terminal)
// + channel stderr (append lines to terminal)
// + channel authenticated (turn status bullet to green)
// + channel disconnected (turn status bullet to red)
//


/*
 * Adds lines into the terminal
 * Supports options: r2n, autoscroll
 */
var append = function(text,isInput) {

  // r2n support (when last line ends with \r, suppress it)
  // TODO: keep content and replace some caracters as in a real terminal?
  if ($("pre:last-child").text().slice(-1).charCodeAt(0)==10) {
    if ($("#r2n").is(':checked')==false) { // remove last
      $("pre:last-child").remove();
    }
  }
  
  // Remove maximum one trailing \n
  if (text.endsWith("\n")) {
    text = text.slice(0,-1);
  }

  // Insert data into terminal line by line as <pre> tags
  var docFrag = document.createDocumentFragment();
  var lines = text.split("\n");
  lines.map(function(line) {
    var pre = document.createElement("pre");
    if (isInput) {
      pre.className = "input";
    }
    pre.onclick = function() {
      var e = $("#textarea");
      if (shiftPressed || $("#forceShift").is(':checked')) {
        e.val(e.val()+line.trim()); // trim to remove \r
      } else {
        e.val(line.trim());
      }
      $("#textarea").focus(); // keep soft keyboard on mobile
    }
    pre.innerHTML = ansi2html(escapeHtml(line));
    docFrag.append(pre);
  });
  $('#terminal').append(docFrag);

  // Autoscroll support
  if ($("#autoscroll").is(':checked')) {
    var h = document.getElementById("terminal").scrollHeight;
    $("#terminal").animate({scrollTop: h},0);
  }
}


/*
 * Queue system to batch render <pre> in terminal
 * Test performances with command du
 */
var queue = "";
var processQueue = function() {
  if (queue) {
    var data = queue;
    queue = "";
    append(data);
  }
}
setInterval(processQueue,10);


// Track state of special keys: shift, alt and ctrl
var shiftPressed = false,
    altPressed = false,
    ctrlPressed = false;
var _history = [];
var iter = 0;


/*
 * Define behavior on resize
 */
$(window).resize(function() {
  $('#terminal').height(
    ($(window).height()
    -$(".banner").outerHeight()
    -$("#textarea").outerHeight()
  )+"px");
});


/*
 * Define behavior on key press
 */
$(function() {
  $(window).on('keydown',function(e) {
    if (e.keyCode == 16) { // shift pressed
      shiftPressed = true;
    } else if (e.keyCode == 17) { // ctrl pressed
      ctrlPressed = true;
    } else if (e.keyCode == 18) { // alt pressed
      altPressed = true;
    } else if (e.key=="c") {
      if (ctrlPressed || $("#forceCtrl").is(':checked')) { // ctrl+C
        console.log("Ctrl+C");
        socket.emit('kill',{}); 
      }
    }
  });
  $(window).on('keyup', function(e) {
    if (e.keyCode == 16) { // shift released
      shiftPressed = false;
    } else if (e.keyCode == 17) { // ctrl released
      ctrlPressed = false;
    } else if (e.keyCode == 18) { // alt released
      altPressed = false;
    }
  });

  $("#textarea").on('keydown', function(e) {
    if (e.keyCode == 13) { // ENTER
      if ((shiftPressed || $("#forceShift").is(':checked'))==false) { // shift+ENTER
        var value = $("#textarea").val();
        append(value+"\n",true);
        _history.push(value);
        iter = _history.length; // index of last added element +1
        socket.emit('input',{message:value});
      }
    } else if (e.keyCode == 38) { // UP
      if ((altPressed || $("#forceAlt").is(':checked'))==false) { // alt+UP
        if (iter) {
          iter--;
          $("#textarea").val(_history[iter]);
        }
      }
    } else if (e.keyCode == 40) { // DOWN
      if ((altPressed || $("#forceAlt").is(':checked'))==false) { // alt+DOWN
        if (iter<_history.length-1) {
          iter++;
          $("#textarea").val(_history[iter]);
        }
      }
    }
  });
  $("#textarea").on('keyup', function(e) {
    if (e.keyCode == 13) { // ENTER
      if ((shiftPressed || $("#forceShift").is(':checked'))==false) // shift+ENTER
        $("#textarea").val("");
    }
  });

  $("#token").on('keydown', function(e) {
    if (e.keyCode == 13) { // ENTER
      authenticate();
      $(".menu.container").hide();
      $("#textarea").focus();
    }
  });

  setInterval(function() { $(window).resize(); }, 100);
});


/*
 * Init socket IO
 */
var socket = io.connect();
var f1 = function(data) { queue += data.message; }
var f2 = function(_class) {
  return function() {
    $(".circleBase").attr("class","left circleBase "+_class);
  }
}
socket.on('stderr', f1);
socket.on('stdout', f1);
socket.on('authenticated', f2("connected"));
socket.on('disconnect', f2("disconnected"));


/*
 * Handle click events
 */
var authenticate = function() {
  var value = $("#token").val();
  socket.emit('authentication',{token:value});
}
var _clear = function() {
  var e = $('#terminal');
  e.text("");
  $("#textarea").focus();
}
var toggleMenu = function() {
  $(".menu.container").toggle();
  $("#token").focus();
}
