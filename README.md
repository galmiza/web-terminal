# Welcome

The project web-terminal is a basic web application that **mimics a shell terminal**.  
It was developed to perform **basic system administration** on a remote server from a mobile phone.

![img](../../downloads/Screenshot_2019-02-05-18-00-13-767_com.ecosia.android_400x.png)
![img](../../downloads/Screenshot_2019-02-05-18-01-14-419_com.ecosia.android_400x.png)

# Features

It does NOT handle:

* xterm
* interactive commands like vi
* ctrl+C command to kill subprocesses

It does support:

* persistent sessions
* multiline commands
* colors
* mobile

## Key shortcuts

Textarea refers to the input box at the bottom on the page to submit commands to the terminal.

* **UP/DOWN** to navigate history
* **Alt+UP/DOWN** to change row in textarea
* **ENTER** to submit content of textarea as commands
* **Shift+ENTER** to append \n at cursor position in textarea
* **Click on a line** to set textarea with the line content
* **Shift+click on a line** to append line content to textarea

## Options

Options are shown in the menu (toggle with top right icon).

* **r2n** converts \r to \n in the incoming lines
* **Autoscroll** when set, the terminal is scrolled down as new lines appear
* **Force key shift** useful on mobile to simulate keeping shift key down
* **Force key alt** useful on mobile to simulate keeping alt key down
* **Force key ctrl** useful on mobile to simulate keeping ctrl key down

## Roadmap

The following features are under development:

* native support of SSL (currently possible through a proxy)
* download/upload files to replace lack of vi

# Install

* Download from the repository

```
git clone https://galmiza@bitbucket.org/galmiza/web-terminal.git
cd web-terminal
vi config.json # edit the listening port and authentication token
```

* Quick and dirty run

```
node main.js   # run the server
```

* Run as a service (with forever)

    * Create file /etc/init.d/web-terminal with content

```
#!/bin/sh

# chkconfig: 35 85 15
# description: web-terminal as a service

case "$1" in
start) sudo -i -u ec2-user sh -c "cd; forever start web-terminal/main.js" ;;
stop) sudo -i -u ec2-user sh -c "cd; forever stop web-terminal/main.js" ;;
restart) $0 stop && $0 start ;;
*)
   echo "Usage: $0 {start|stop|restart}"
esac

exit 0
```

    * Register the service:

```
sudo chmod +x /etc/init.d/web-terminal
sudo chkconfig web-terminal on
```

    * Start the service

```
sudo service web-terminal start
```

Recommendations

* run with forever
* run as a service
* use with a proxy (to bring SSL)


# How does it work

In short:

Commands issued from the web application are sent to the server using websockets.  
These commands are forwarded into the input stream (stdin) of a bash process running on the server.  
Output and errors (resp. stdout and stderr) from this process are sent back the web application using websockets.

# Alternatives

If you need a web terminal that supports xterm, render vi and much more, check the excellent tty.js.  
https://github.com/chjj/tty.js

# Test

```
du
N=10; while [ $N -ne 0 ]; do N=$((N-1)); echo -ne "$N \r"; sleep 1; done
echo "<h1>yep'</h1>"
for x in {0..8}; do for i in {30..37}; do for a in {40..47}; do echo -ne "\e[$x;$i;$a""m\\\e[$x;$i;$a""m\e[0;37;40m "; done; echo; done; done; echo ""
```