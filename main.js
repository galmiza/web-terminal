/*
 * Web terminal
 *  Authenticated socket clients are attached to a bash process
 *  Commands received on the socket are forwarded into the stdin stream of the process
 *  Stdout and stderr streams from the process is sent back to the socket
 */

// Dependencies
var express = require('express');
var fs = require('fs');
var process = require('process');
var io = require('socket.io');
var path = require('path');
var { spawn } = require('child_process');

// Read configuration
var config = JSON.parse(fs.readFileSync(__dirname+'/config.json', 'utf8'));

// Serve client webapp
var app = express();
app.use('/', express.static(path.join(__dirname, 'public')));

// Run app
var server = app.listen(config.port, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Listening at http://%s:%s", host, port)
});

// Handle socket.io
var listener = io.listen(server);
listener.sockets.on('connection', function(socket) {
  socket.authenticated = false;
  socket.on('authentication', function(data) {
    if (data.token == config.token) {
    	socket.authenticated = true;
    	var p = spawn('bash');
      socket.process = p;
    	p.stdin.setEncoding('utf-8');
      p.stdout.on('data', function(data) {
        socket.emit('stdout', {'message': String(data) });
      });
      p.stderr.on('data', function(data) {
        socket.emit('stderr', {'message': String(data) });
      });
      p.on('close', function(code) {
        socket.close();
      });
      socket.emit('authenticated',{});
    }
  });
  socket.on('input', function(data) {
  	if (socket.authenticated==false)
  	  return socket.emit('stderr', {'message': "Not authenticated\n" });

  	data.message.split("\n").map(function(line) {
      //var p = spawn('bash',['-c',line]);
      //socket.child = p;
      //socket.process.stdin.pipe(p.stdin);
      socket.process.stdin.write(line+"\n");
  	});
  });
  socket.on('kill', function(data) {
    //socket.process.stdin.pause();
    //socket.process.kill(process.SIGCONT);
    socket.process.kill(process.SIGINT);
  });
});